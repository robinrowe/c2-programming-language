# C2 Programming Language

* Author: Robin Rowe
* License: Same as picoc and tinycc

## Description

### C2 Programming Language Features

C2 is memory safe language (MSL) with syntax closely similar to C. C2 exists to increase the security of C or C-like C++ code without the cost of rewriting existing programs in a different programming language, such as Rust, or the cost of retraining C/C++ programmers in a new language with an unfamiliar syntax. Although C2 reuses some features and syntax from C++, it keeps the small-and-simple design goals of C.

### What C2 Has that C Has:

Almost the same syntax as C, plus a few enhancements for safety.

### What C2 Doesn’t Have that C Has:

1. No buffer overruns, all pointer math checked
2. No malloc/free, so no memory use after deallocation
3. No unchecked void*, pointers are made typesafe including void*

### What Only C2 Has, Not in C, Not in C++:

1. Memory safety, pointers are checked by compiler for overruns
2. Protected memory allocator, for separate password high security memory
3. Thread local memory
4. errno_t
5. typesafe printf()
6. typesafe void*
7. trap keyword for error handling, simpler than exceptions
8. Owned pointers, no free()
9. zero_on_free(true/false)
10. Variable initialization required
11. Call stack for backtrace()
12. read_only keyword
13. Typesafe containers based on glib

### What C2 Has that C++ Has:

1. Constructors
2. Destructors
3. Default parameters to function
4. Member functions
5. Const correct member functions
6. structs are types, no need to typedef struct

### What C2 Doesn’t Have that C++ Has:

1. No name-mangling 
2. No function overloading
3. No operator overloading
4. No inheritance
5. No templates
6. No exceptions
7. No public/protected/private
8. No iostreams
9. No STL

## For Comparison, an Example of Unsafe C Code

```c
// foo.c
// from https://www.bugseng.com/sites/default/files/resources/C-rusted_a_Formally_Verifiable_Flavor_of_C.pdf

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int foo(const char *fname, size_t bufsize) 
{	int fd = open(fname, O_RDONLY);
	char *buf = malloc(bufsize);
	ssize_t bytes = read(fd, buf, bufsize);
	buf[bytes] = '\0';
	return 0;
}

```

## Same Example in Memory Safe C2

```c

#include <file_t.h>
#include <rand.h>

int foo(const char *filename, size_t bufsize) 
{	file_t ft(filename,O_RDONLY,bufsize);
	ft.read();
	write(stdout,ft.buf,ft.buf.bytes);
	ft.buf[ft.buf.size] = 0;//compile-time buffer overrun
	ft.buf[rand()] = 0;//runtime buffer overrun if >= ft.buf.size
	return 0;
}

/* output: buffer overrun here is compile-time error, not this...

hello world

FATAL ERROR: "buffer overrun"
Path: /code/foo/test_foo/build/debug/test_foo.exe
Call stack:
 main
  foo

*/

```

## C2 Include Files

```c
struct memory_t
{	read_only void* buffer;
	read_only size_t size;
	read_only void (*func)(void*) ctor;
	read_only void (*func)(void*) dtor;
	void open(size_t size,size_t bufsize = sizeof(*(void*))) !read_only
	{	buffer = malloc(size * bufsize);
		if(!buffer)
		{	return trap.error(outofmem,"out of memory");
		}
		.size = size;
		for(void* p:buffer)
		{	trap ctor(p);
	}	}
	memory_t()
	{	.zero();// same as this->zero() in C++
		ctor = memory();
		dtor = ~memory();
	}
	~memory_t()
	{	for(void* p:buffer)
		{	trap dtor(p);
		}
		free(buffer);
	}
};

struct thread_local_memory_t;
struct protected_memory_t;

struct file_t
{	read_only int fd;
	read_only const char* filename;
	errno_t err;
	memory_t<char*> buf;
	size_t bytes_read;
	void open(const char *fname,int flags=O_RDONLY,size_t bufsize) !read_only
	{	fd = open(fname,flags);
		if(fd <= 0)
		{	return trap.error(enoent,"file not found");
		}
		trap buf.open(bufsize);
	}
	void close() !read_only
	{	if(fd>0)
		{	close(fd);
			fd=0;
		}
		buffer.close();
	}
	void read()
	{	bytes_read = read(fd, buf, buf.size);
		if(!bytes_read)
		{	return;// eof
		}
		if(bytes_read < 0)
		{	return trap.error(errno,"read error");
	}	}	
	file_t(const char *filename=0,int flags=O_RDONLY,size_t bufsize=0)
	{	.zero();// bzero any data that has no constructor
		if(!filename)
		{	return;
		}
		trap open(filename,bufsize);
	}
	~file_t()
	{	.close();
	}
};

```

## typesafe printf()

```c

#include "point.h"

int main()
{	point_t pt(10,10);
	printf("pt = %{}\n",&pt);
	return 0;
}

// point_t.h

struct point_t
{	int x;
	int y;
	point_t(int x=0,int y=0)
	{	.x = x;
		.y = y;
	}
	size_t sprintf(string_t* s)
	{	const size_t len = snprintf(s->text,s->size,"%{},%{}",x,y);
		if(s.size > len)
		{	return;
		}
		s[size-i] = 0;
		trap.warn(esize,"sprintf overflow");
		return len;
}	};

// string_t.h

struct string_t
{	read_only size_t size = MAX_STRING_SIZE;
	size_t length;
	char buffer[size];
	string_t(const char* s = 0)
	{	if(!s)
		{	buffer[0] = 0;
			length = 0;
			return;
		}
		.copy(s);
	}
	void copy(const char* s,size_t offset = 0)
	{	length = str_copy(buffer,offset,size);
	}
	void append(const char* s)
	{	.copy(s,length);
	}
	bool is_blank()
	{	return 0 == length;
	}
};

```
## C2 Progress

* First good Readme written 2024/4/19
* Feedback at Tokyo wg21 conference
* gitlab C2 repo created
* Missed France wg14 conference
* Feedback from wg14 and wg21 mailing lists

## C2 Roadmap

* Write docs, including code examples
* Enhance picoc
* Enhance tinycc